# Generated by Django 3.0.4 on 2021-09-04 17:56

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import martor.models
import simple_history.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('people', '0028_auto_20210222_2326'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='institution',
            name='dept',
        ),
        migrations.RemoveField(
            model_name='institution',
            name='dept_website',
        ),
        migrations.RemoveField(
            model_name='institution',
            name='inst',
        ),
        migrations.RemoveField(
            model_name='institution',
            name='inst_website',
        ),
        migrations.AddField(
            model_name='institution',
            name='city',
            field=models.CharField(default='Cambridge', help_text='City in which institution is located.', max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='institution',
            name='country',
            field=models.CharField(default='US', help_text='Country in which institution is located.', max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='institution',
            name='name',
            field=models.CharField(blank=True, help_text='Name of the institution.', max_length=150, null=True),
        ),
        migrations.AddField(
            model_name='institution',
            name='parent',
            field=models.ForeignKey(blank=True, help_text="Parent institution of institution (E.g., 'MIT' is the parent of 'DUSP').", null=True, on_delete=django.db.models.deletion.SET_NULL, to='people.Institution'),
        ),
        migrations.AddField(
            model_name='institution',
            name='postal',
            field=models.CharField(blank=True, default='', help_text='Postcode in which institution is located.', max_length=20),
        ),
        migrations.AddField(
            model_name='institution',
            name='room',
            field=models.CharField(blank=True, default='', help_text='Room at which the institution can be reached.', max_length=20),
        ),
        migrations.AddField(
            model_name='institution',
            name='state',
            field=models.CharField(default='MA', help_text='State in which institution is located.', max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='institution',
            name='website',
            field=models.URLField(blank=True, default='', help_text="Institution's website."),
        ),
        migrations.AlterField(
            model_name='person',
            name='slug',
            field=models.SlugField(default='', editable=False, unique=True),
        ),
        migrations.AlterField(
            model_name='person',
            name='title',
            field=models.CharField(choices=[('D', 'Director'), ('A', 'Affiliate'), ('C', 'Contributor'), ('R', 'Research Assistant'), ('F', 'Faculty Steering Committee'), ('L', 'Alumnus'), ('T', 'Teaching Assistant')], default='', max_length=1),
        ),
        migrations.CreateModel(
            name='HistoricalPerson',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('photo', models.TextField(blank=True, max_length=100, null=True)),
                ('first', models.CharField(max_length=25)),
                ('middle', models.CharField(blank=True, default='', max_length=25)),
                ('last', models.CharField(max_length=25)),
                ('cred', models.CharField(blank=True, choices=[('MCP', 'Master of City Planning'), ('PhD', 'Doctor of Philosophy'), ('MUP', 'Master of Urban Planning'), ('MURP', 'Master of Urban and Regional Planning'), ('MFA', 'Master of Fine Arts'), ('MLA', 'Master of Landscape Architecture'), ('MArch', 'Master of Architecture'), ('MBA', 'Master of Business Administration'), ('MPA', 'Master of Public Administration'), ('MDes', 'Master of Design Studies'), ('DDes', 'Doctor of Design')], default='PhD', max_length=5, null=True)),
                ('email', models.EmailField(blank=True, default='', max_length=254)),
                ('website', models.URLField(blank=True, default='')),
                ('pronouns', models.CharField(choices=[('M', 'He/him/his'), ('W', 'She/her/hers'), ('N', 'He/him/his or They/them/theirs'), ('H', 'She/her/hers or They/them/theirs'), ('T', 'They/them/theirs')], default='T', max_length=1)),
                ('title', models.CharField(choices=[('D', 'Director'), ('A', 'Affiliate'), ('C', 'Contributor'), ('R', 'Research Assistant'), ('F', 'Faculty Steering Committee'), ('L', 'Alumnus'), ('T', 'Teaching Assistant')], default='', max_length=1)),
                ('bio', martor.models.MartorField(blank=True, max_length=3000)),
                ('orcid', models.CharField(blank=True, default='', max_length=19)),
                ('pgp', models.CharField(blank=True, default='', max_length=50)),
                ('twitter', models.CharField(blank=True, default='', max_length=50)),
                ('gitlab', models.CharField(blank=True, default='', max_length=25)),
                ('github', models.CharField(blank=True, default='', max_length=25)),
                ('zotero', models.CharField(blank=True, default='', max_length=25)),
                ('linkedin', models.CharField(blank=True, default='', max_length=25)),
                ('created_at', models.DateTimeField(blank=True, editable=False)),
                ('modified_at', models.DateTimeField(blank=True, editable=False)),
                ('vita', models.TextField(blank=True, default='', max_length=100)),
                ('slug', models.SlugField(default='', editable=False)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('django_user', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'historical person',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalInstitution',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('name', models.CharField(blank=True, help_text='Name of the institution.', max_length=150, null=True)),
                ('short', models.CharField(blank=True, max_length=150, null=True)),
                ('abbr', models.CharField(blank=True, max_length=10, null=True)),
                ('room', models.CharField(blank=True, default='', help_text='Room at which the institution can be reached.', max_length=20)),
                ('city', models.CharField(help_text='City in which institution is located.', max_length=100)),
                ('state', models.CharField(help_text='State in which institution is located.', max_length=100)),
                ('postal', models.CharField(blank=True, default='', help_text='Postcode in which institution is located.', max_length=20)),
                ('country', models.CharField(help_text='Country in which institution is located.', max_length=100)),
                ('website', models.URLField(blank=True, default='', help_text="Institution's website.")),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('parent', models.ForeignKey(blank=True, db_constraint=False, help_text="Parent institution of institution (E.g., 'MIT' is the parent of 'DUSP').", null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='people.Institution')),
            ],
            options={
                'verbose_name': 'historical institution',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
    ]
