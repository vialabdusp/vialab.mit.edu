from django.contrib import admin
from .models import Event, EventMetadata, Venue, Semester, Days, Role

# admin.site.register(Person, PersonAdmin)

class RoleInline(admin.TabularInline):
    model = Role
    extra = 1

class PersonInline(admin.ModelAdmin):
    inlines = (RoleInline,)

admin.site.register(Event, PersonInline, save_as = True)
admin.site.register(EventMetadata)
admin.site.register(Venue, save_as = True)
admin.site.register(Semester, save_as = True)
admin.site.register(Days, save_as = True)